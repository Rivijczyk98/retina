﻿using System;
using System.Drawing;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = System.Drawing.Color;

namespace Retina
{
    class ThinningLogic
    {
        Bitmap bitmap;

        int[,] binarized;
        static int[] weights = { 1, 2, 4, 8, 16, 32, 64, 128 };

        static int[] deleteWeightsFour = { 3, 6, 24, 96, 129, 7, 12, 48, 192, 14, 28, 56, 112, 224, 193, 131, 15, 30, 60, 120, 240, 225, 195, 135 };

        static int[] deleteWeightsRest = { 3, 5, 7, 12, 13, 14, 15, 20,
                                21, 22, 23, 28, 29, 30, 31, 48,
                                52, 53, 54, 55, 56, 60, 61, 62,
                                63, 65, 67, 69, 71, 77, 79, 80,
                                81, 83, 84, 85, 86, 87, 88, 89,
                                91, 92, 93, 94, 95, 97, 99, 101,
                                103, 109, 111, 112, 113, 115, 116, 117,
                                118, 119, 120, 121, 123, 124, 125, 126,
                                127, 131, 133, 135, 141, 143, 149, 151,
                                157, 159, 181, 183, 189, 191, 192, 193,
                                195, 197, 199, 205, 207, 208, 209, 211,
                                212, 213, 214, 215, 216, 217, 219, 220,
                                221, 222, 223, 224, 225, 227, 229, 231,
                                237, 239, 240, 241, 243, 244, 245, 246,
                                247, 248, 249, 251, 252, 253, 254, 255 };

        public ThinningLogic(Bitmap image)
        {
            bitmap = image;
        }

        public Bitmap GetFinalResult() => bitmap;

        public void ThinningOperation()
        {
            int wid = bitmap.Width;
            int hei = bitmap.Height;
            binarized = new int[wid, hei];
            int[,] temporary = new int[wid, hei];

            for (int i = 0; i < wid; i++)
            {
                for (int j = 0; j < hei; j++)
                {
                    binarized[i, j] = (bitmap.GetPixel(i, j).R == 0) ? 1 : 0;
                }
            }
            while (!stopThinning(binarized, temporary))
            {
                for (int i = 0; i < wid; i++)
                    for (int j = 0; j < hei; j++)
                        temporary[i, j] = binarized[i, j];

                for (int i = 0; i < wid; i++)
                {
                    for (int j = 0; j < hei; j++)
                    {
                        if (binarized[i, j] != 0) binarized[i, j] = setValue(i, j);

                        if (binarized[i, j] == 2) binarized[i, j] = determineToDelete(binarized[i, j], i, j, deleteWeightsFour);
                    }
                }
                reducePoints();
                for (int i = 0; i < wid; i++)
                    for (int j = 0; j < hei; j++)
                        binarized[i, j] = Math.Min(binarized[i, j], 1);
            }
            saveImage();
        }

        private void saveImage()
        {
            Bitmap b = new Bitmap(bitmap);
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    byte val = (binarized[i, j] == 1) ? (byte)0 : (byte)255;
                    Color color = Color.FromArgb(val, val, val);
                    b.SetPixel(i, j, color);
                }
            }
            bitmap = b;
        }

        private void reducePoints()
        {
            for (int i = 0; i < bitmap.Width; i++)
                for (int j = 0; j < bitmap.Height; j++)
                    if (binarized[i, j] == 2) binarized[i, j] = determineToDelete(binarized[i, j], i, j, deleteWeightsRest);

            for (int i = 0; i < bitmap.Width; i++)
                for (int j = 0; j < bitmap.Height; j++)
                    if (binarized[i, j] == 3) binarized[i, j] = determineToDelete(binarized[i, j], i, j, deleteWeightsRest);
        }

        private int determineToDelete(int point, int i, int j, int[] toDelete)
        {
            int[] k = { i, i + 1, i + 1, i + 1, i, i - 1, i - 1, i - 1 };
            int[] l = { j - 1, j - 1, j, j + 1, j + 1, j + 1, j, j - 1 };
            int value = 0;

            for (int a = 0; a < 8; a++)
            {
                if (k[a] < 0 || k[a] >= bitmap.Width || l[a] < 0 || l[a] >= bitmap.Height) continue;
                int x = Math.Min(binarized[k[a], l[a]], 1);
                value += x * weights[a];
            }
            foreach (var single in toDelete)
            {
                if (single == value)
                {
                    point = 0;
                }
            }
            return point;
        }

        private int setValue(int i, int j)
        {
            if (i > 0 && binarized[i - 1, j] == 0) return 2;
            if (j > 0 && binarized[i, j - 1] == 0) return 2;
            if (i < bitmap.Width - 1 && binarized[i + 1, j] == 0) return 2;
            if (j < bitmap.Height - 1 && binarized[i, j + 1] == 0) return 2;

            if (i > 0 && j > 0 && binarized[i - 1, j - 1] == 0) return 3;
            if (i < bitmap.Width - 1 && j > 0 && binarized[i + 1, j - 1] == 0) return 3;
            if (i > 0 && j < bitmap.Height - 1 && binarized[i - 1, j + 1] == 0) return 3;
            if (i < bitmap.Width - 1 && j < bitmap.Height - 1 && binarized[i + 1, j + 1] == 0) return 3;

            return 1;
        }

        private bool stopThinning(int[,] arr1, int[,] arr2)
        {
            for (int i = 0; i < bitmap.Width; i++)
                for (int j = 0; j < bitmap.Height; j++)
                    if (arr1[i, j] != arr2[i, j]) return false;
            return true;
        }


    }
}
