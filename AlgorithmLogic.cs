﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = System.Drawing.Color;

namespace Retina
{
    class AlgorithmLogic
    {
        public Bitmap imagetoedit, temporaryBitmap;
        int[,] binarized;
        public List<Tuple<int, int>> foundPoints;

        public AlgorithmLogic(Bitmap image)
        {
            imagetoedit = new Bitmap(image);
        }

        public Bitmap GetFinalResult() => imagetoedit;

        public List<Tuple<int, int>> GetFoundPoints() => foundPoints;

        public void AlgorithmOperation()
        {
            Bitmap image = new Bitmap(imagetoedit);
            int wid = imagetoedit.Width;
            int hei = imagetoedit.Height;
            binarized = new int[wid, hei];
            int[,] temporary = new int[wid, hei];

            for (int i = 0; i < wid; i++)
            {
                for (int j = 0; j < hei; j++)
                {
                    binarized[i, j] = (image.GetPixel(i, j).R == 0) ? 1 : 0;
                }
            }

            temporaryBitmap = new Bitmap(image);
            foundPoints = new List<Tuple<int, int>>();

            for (int i = 1; i < wid - 1; i++)
            {
                for (int j = 1; j < hei - 1; j++)
                {
                    if (binarized[i, j] == 0) continue;

                    int[] k = { i + 1, i + 1, i, i - 1, i - 1, i - 1, i, i + 1, i + 1 };
                    int[] l = { j, j - 1, j - 1, j - 1, j, j + 1, j + 1, j + 1, j };

                    int CN = 0;

                    for (int a = 0; a < 8; a++)
                    {
                        int val_a = binarized[k[a], l[a]], val_b = binarized[k[a + 1], l[a + 1]];
                        CN += Math.Abs(val_a - val_b);
                    }

                    CN /= 2;

                    if (CN != 2)
                    {
                        foundPoints.Add(new Tuple<int, int>(i, j));
                        //drawPoint(i, j, imagetoedit);
                    }
                }
            }

            //MessageBox.Show(foundPoints.Count.ToString());

        }

        private void drawPoint(int i, int j, Bitmap imagetoedit)
        {
            Bitmap image = new Bitmap(imagetoedit);
            int wid = imagetoedit.Width;
            int hei = imagetoedit.Height;
            int[] k = { i - 1, i, i + 1,
                i - 2, i - 1, i, i + 1, i + 2,
                i - 3, i - 2, i - 1, i, i + 1, i + 2, i + 3,
                i - 3, i - 2, i - 1, i, i + 1, i + 2, i + 3,
                i - 3, i - 2, i - 1, i, i + 1, i + 2, i + 3,
                i - 2, i - 1, i, i + 1, i + 2,
                i - 1, i, i + 1 };
            int[] l = { j - 3, j - 3, j - 3,
                j - 2, j - 2, j - 2, j - 2, j - 2,
                j - 1, j - 1, j - 1, j - 1, j - 1, j - 1, j - 1,
                j, j, j, j, j, j, j,
                j + 1, j + 1, j + 1, j + 1, j + 1, j + 1, j + 1,
                j + 2, j + 2, j + 2, j + 2, j + 2,
                j + 3, j + 3, j + 3 };

            for (int a = 0; a < k.Count(); a++)
            {
                if (k[a] < 0 || k[a] >= wid || l[a] < 0 || l[a] >= hei) continue;

                if (imagetoedit.GetPixel(k[a], l[a]).R == 0) continue;

                Color color = Color.FromArgb(235, 30, 35);
                imagetoedit.SetPixel(k[a], l[a], color);
            }
        }

        private void saveImage()
        {
            Bitmap img = new Bitmap(imagetoedit);
            for (int i = 0; i < imagetoedit.Width; i++)
            {
                for (int j = 0; j < imagetoedit.Height; j++)
                {
                    byte val = (binarized[i, j] == 1) ? (byte)0 : (byte)255;
                    Color color = Color.FromArgb(val, val, val);
                    img.SetPixel(i, j, color);
                }
            }
            imagetoedit = img;
        }



    }
}
