﻿using Retina;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq.Expressions;
using System.Windows.Documents;

public class RetinaAlgorithm
{
    private Bitmap image;
    private Bitmap greyscaled;

    public RetinaAlgorithm(Bitmap _image)
    {
        this.image = _image;
    }

    public void Start()
    {
        ConvertToGreyScale();
        BlurImage();
        SubtractImages();
        OtsuBinarization();
        MedianFilter();
        ClosingMorphology();
        K3M();
        FindingM();
    }

    public Bitmap GetFinalImage() => image;

    /**
     * Konwersja na skalę szarości
     */
    public void ConvertToGreyScale()
    {
        greyscaled = new Bitmap(image.Width, image.Height);

        for (int i = 0; greyscaled.Width > i; i++)
        {
            for (int j = 0; greyscaled.Height > j; j++)
            {
                var pixel = image.GetPixel(i, j);
                //int R = pixel.R;
                //int G = pixel.G;
                //int B = pixel.B;
                //int val = (R + G + B) / 3;
                int val = pixel.G;
                greyscaled.SetPixel(i, j, Color.FromArgb(val, val, val));
            }
        }

        image = greyscaled;
    }

    /**
     * Rozmywanie obrazu
     */
    public void BlurImage()
    {
        int[,] mask = { 
            { 1, 1, 1, 1, 1, 1, 1 }, 
            { 1, 1, 1, 1, 1, 1, 1 }, 
            { 1, 1, 1, 1, 1, 1, 1 }, 
            { 1, 1, 1, 1, 1, 1, 1 }, 
            { 1, 1, 1, 1, 1, 1, 1 }, 
            { 1, 1, 1, 1, 1, 1, 1 }, 
            { 1, 1, 1, 1, 1, 1, 1 } };

        Bitmap bitmap = new Bitmap(image.Width, image.Height);

        for (int x = 0; x < image.Width; x++)
        {
            for (int y = 0; y < image.Height; y++)
            {
                bitmap.SetPixel(x, y, BluringGetPixel(image, x, y, mask));
            }
        }

        image = bitmap;
    }

    private Color BluringGetPixel(Bitmap b, int x, int y, int[,] mask)
    {
        double sumR = 0;
        int visited = 0;

        int beginX = x - mask.GetLength(0) / 2, beginY = y - mask.GetLength(0) / 2;
        int end = mask.GetLength(0);

        for (int i = 0; i < end; i++)
        {
            for (int j = 0; j < end; j++)
            {
                if(beginX + i >= 0 && beginY + j >= 0 && beginX + i < b.Width && beginY + j < b.Height) 
                { 
                    sumR += b.GetPixel(beginX + i, beginY + j).R * mask[i, j];
                    visited++;
                } 
                
            }
        }

        int r = (int)(sumR/visited);

        if (r < 0)
        {
            r = 0;
        }
        else if (r > 255)
        {
            r = 255;
        }

        return Color.FromArgb(r,r,r);
    }

    /**
     * Odejmowanie od siebie obrazów
     */
    public void SubtractImages()
    {
        Bitmap bitmap = new Bitmap(image.Width, image.Height);

        for(int i = 0; i < image.Width; i++)
        {
            for(int j = 0; j < image.Height; j++)
            { 
                int val = image.GetPixel(i, j).R - greyscaled.GetPixel(i, j).R;
                if (val < 0) val = 0;
                bitmap.SetPixel(i, j, Color.FromArgb(val, val, val));
            }
        }

        image = bitmap;
    }

    /**
     * OTSU
     **/
    public void OtsuBinarization()
    {
        int[] h = getHistogram(image);

        float p1, p2, p12;
        float max = -1, threshold = 0;

        for (int i = 0; i < 256; i++)
        {
            p1 = px(0, i, h);
            p2 = px(i + 1, 255, h);

            p12 = p1 * p2;

            if (p12 == 0) p12 = 1;

            float diff = (mx(0, i, h) * p2) - (mx(i + 1, 255, h) * p1);

            float v = (float)diff * diff / p12;
            if (v > max)
            {
                max = v;
                threshold = i;
            }
        }

        for (int x = 0; x < image.Width; x++)
        {
            for (int y = 0; y < image.Height; y++)
            {
                var pixel = image.GetPixel(x, y).R;
                if (pixel < threshold)
                {
                    image.SetPixel(x, y, Color.Black);
                }
                else
                {
                    image.SetPixel(x, y, Color.White);
                }
            }
        }
    }

    private int[] getHistogram(Bitmap b)
    {
        int[] hist = new int[256];

        for (int i = 0; b.Width > i; i++)
        {
            for (int j = 0; b.Height > j; j++)
            {
                var pixel = b.GetPixel(i, j).R;
                hist[pixel]++;
            }
        }

        return hist;
    }
    
    private float px(int start, int end, int[] histogram)
    {
        int sum = 0;

        for (int i = start; i < end; i++)
        {
            sum += histogram[i];
        }

        return (float)sum;
    }

    private float mx(int start, int end, int[] histogram)
    {
        int sum = 0;
        int i;

        for (i = start; i < end; i++)
            sum += i * histogram[i];

        return (float)sum;
    }

    /**
     * Medianowy
     */
    public void MedianFilter()
    {
        Bitmap bitmap = new Bitmap(image.Width, image.Height);

        int[] maskR = new int[9];
        int[] maskG = new int[9];
        int[] maskB = new int[9];

        for (int x = 0; x < image.Width; x++)
        {
            for (int y = 0; y < image.Height; y++)
            {
                if (x == 0 || y == 0 || x == image.Width - 1 || y == image.Height - 1)
                {
                    bitmap.SetPixel(x, y, image.GetPixel(x, y));
                    continue;
                }

                int counter = 0;

                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        var pixel = image.GetPixel(x - 1 + i, y - 1 + j);

                        maskR[counter] = pixel.R;
                        maskG[counter] = pixel.G;
                        maskB[counter] = pixel.B;

                        counter++;
                    }
                }

                Array.Sort(maskR);
                Array.Sort(maskG);
                Array.Sort(maskB);

                bitmap.SetPixel(x, y, Color.FromArgb(maskR[4], maskG[4], maskB[4]));
            }
        }

        image = bitmap;
    }

    /**
     * Zamknięcie morfologiczne
     */
    public void ClosingMorphology()
    {
        image = Dilation(image);
        image = Erosion(image);
    }

    private Bitmap Erosion(Bitmap b)
    {
        Bitmap bitmap = new Bitmap(b.Width, b.Height);

        for (int i = 0; i < b.Width; i++)
        {
            for (int j = 0; j < b.Height; j++)
            {
                int neighbors = 0;
                for (int k = i - 1; k <= i + 1; k++)
                {
                    for (int n = j - 1; n <= j + 1; n++)
                    {
                        if (k == i && n == j) continue;
                        if (k >= 0 && n >= 0 && k < b.Width && n < b.Height)
                        {
                            if (b.GetPixel(k, n).R == 255)
                            {
                                neighbors++;
                            }
                            else continue;
                        }
                    }
                }
                if (neighbors == 0) bitmap.SetPixel(i, j, Color.Black);
                else bitmap.SetPixel(i, j, Color.White);
            }
        }
        return bitmap;
    }

    private Bitmap Dilation(Bitmap b)
    {
        Bitmap bitmap = new Bitmap(b.Width, b.Height);

        for (int i = 0; i < b.Width; i++)
        {
            for(int j = 0; j < b.Height; j++)
            {
                int neighbors = 0;
                for(int k = i - 1; k <= i + 1; k++)
                {
                    for(int n = j - 1; n <= j + 1; n++)
                    {
                        if(k >= 0 && n >= 0 && k < b.Width && n < b.Height) { 
                            if (b.GetPixel(k, n).R == 255)
                            {
                                neighbors++;
                            }
                            else continue;
                        }
                    }
                }
                if (neighbors <= 1) bitmap.SetPixel(i, j, Color.White);
                else bitmap.SetPixel(i, j, Color.Black);
            }
        }
        return bitmap;
    }

    /**
     * K3M scienianie
     * TODO PRZEKOPIOWAĆ
     */
    public void K3M()
    {
        ThinningLogic thinning = new ThinningLogic(image);
        thinning.ThinningOperation();
        image = thinning.GetFinalResult();
    }

    /**
     * Wyszukiwanie minucji
     */
    public void FindingM()
    {
        AlgorithmLogic algorithmLogic = new AlgorithmLogic(image);
        algorithmLogic.AlgorithmOperation();
        //image = algorithmLogic.GetFinalResult();
        DeleteM(algorithmLogic.GetFoundPoints());
    }

    /**
     * Usuwanie fałszywych minucji
     */
    public void DeleteM(List<Tuple<int, int>> foundPoints)
    {
        FilterLogic filterLogic = new FilterLogic(image, 10.ToString(), foundPoints);
        filterLogic.FilterOperation();
        image = filterLogic.GetFinalResult();
    }
}
