﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Color = System.Drawing.Color;

namespace Retina
{
    class FilterLogic
    {
        public Bitmap imagetoedit, temporaryBitmap;
        int[,] binarized;
        List<Tuple<int, int>> foundPoints;
        string text;

        public FilterLogic(Bitmap image, string textnumber, List<Tuple<int, int>> points)
        {
            imagetoedit = new Bitmap(image);
            text = textnumber;
            foundPoints = points;
        }

        public Bitmap GetFinalResult() => imagetoedit;

        public void FilterOperation()
        {
            Bitmap image = new Bitmap(imagetoedit);
            int wid = imagetoedit.Width;
            int hei = imagetoedit.Height;
            binarized = new int[wid, hei];
            int[,] temporary = new int[wid, hei];
            Bitmap cloned = new Bitmap(image);


            List<Tuple<int, int>> filteredPoints = new List<Tuple<int, int>>();
            for (int i = 0; i < foundPoints.Count; i++)
            {
                bool canInsert = true;
                int x1 = foundPoints[i].Item1, y1 = foundPoints[i].Item2;
                for (int j = 0; j < foundPoints.Count; j++)
                {
                    if (i == j) continue;
                    int x2 = foundPoints[j].Item1, y2 = foundPoints[j].Item2;
                    double dist = Math.Sqrt(Math.Pow((x1 - x2), 2) + Math.Pow((y1 - y2), 2));
                    if (dist < int.Parse(text))
                    {
                        // filter_distance.Text
                        canInsert = false;
                        break;
                    }
                }

                if (canInsert)
                {
                    bool left = checkOnLeft(x1, y1, cloned), right = checkOnRight(x1, y1, cloned), up = checkOnUp(x1, y1, cloned), down = checkOnDown(x1, y1, cloned);
                    if (left && up || up && right || right && down || down && left) continue;
                    filteredPoints.Add(foundPoints[i]);
                    drawPoint(foundPoints[i].Item1, foundPoints[i].Item2, imagetoedit);
                }
            }

            MessageBox.Show(filteredPoints.Count.ToString());
        }

        private bool checkOnLeft(int i, int j, Bitmap bmp)
        {
            for (int x = i - 1; x > 0; x--)
            {
                Color c = bmp.GetPixel(x, j);
                if (c.R == 0) return false;
            }
            return true;
        }

        private bool checkOnRight(int i, int j, Bitmap bmp)
        {
            for (int x = i + 1; x < bmp.Width; x++)
            {
                Color c = bmp.GetPixel(x, j);
                if (c.R == 0) return false;
            }
            return true;
        }

        private bool checkOnUp(int i, int j, Bitmap bmp)
        {
            for (int x = j - 1; x > 0; x--)
            {
                Color c = bmp.GetPixel(i, x);
                if (c.R == 0) return false;
            }
            return true;
        }

        private bool checkOnDown(int i, int j, Bitmap bmp)
        {
            for (int x = j + 1; x < bmp.Height; x++)
            {
                Color c = bmp.GetPixel(i, x);
                if (c.R == 0) return false;
            }
            return true;
        }

        private void drawPoint(int i, int j, Bitmap imagetoedit)
        {
            Bitmap image = new Bitmap(imagetoedit);
            int wid = imagetoedit.Width;
            int hei = imagetoedit.Height;
            int[] k = { i - 1, i, i + 1,
                i - 2, i - 1, i, i + 1, i + 2,
                i - 3, i - 2, i - 1, i, i + 1, i + 2, i + 3,
                i - 3, i - 2, i - 1, i, i + 1, i + 2, i + 3,
                i - 3, i - 2, i - 1, i, i + 1, i + 2, i + 3,
                i - 2, i - 1, i, i + 1, i + 2,
                i - 1, i, i + 1 };
            int[] l = { j - 3, j - 3, j - 3,
                j - 2, j - 2, j - 2, j - 2, j - 2,
                j - 1, j - 1, j - 1, j - 1, j - 1, j - 1, j - 1,
                j, j, j, j, j, j, j,
                j + 1, j + 1, j + 1, j + 1, j + 1, j + 1, j + 1,
                j + 2, j + 2, j + 2, j + 2, j + 2,
                j + 3, j + 3, j + 3 };

            for (int a = 0; a < k.Count(); a++)
            {
                if (k[a] < 0 || k[a] >= wid || l[a] < 0 || l[a] >= hei) continue;

                if (imagetoedit.GetPixel(k[a], l[a]).R == 0) continue;

                Color color = Color.FromArgb(235, 30, 35);
                imagetoedit.SetPixel(k[a], l[a], color);
            }
        }
        private void saveImage()
        {
            Bitmap img = new Bitmap(imagetoedit);
            for (int i = 0; i < imagetoedit.Width; i++)
            {
                for (int j = 0; j < imagetoedit.Height; j++)
                {
                    byte val = (binarized[i, j] == 1) ? (byte)0 : (byte)255;
                    Color color = Color.FromArgb(val, val, val);
                    img.SetPixel(i, j, color);
                }
            }
            imagetoedit = img;
        }
    }
}
