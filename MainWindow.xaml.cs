﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using Microsoft.Win32;
using System.IO;

namespace Retina
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Bitmap bitmap;
        private BitmapImage bitmapImage;
        private double size = 1.0;
        private string url;

        private int step = 0;
        private RetinaAlgorithm retina;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Title = "Select an image";
            openFileDialog.Filter = "All supported graphics|*.jpg;*.gif;*.tif;*png|" +
                "JPEG (*.jpg)|*.jpg|" +
                "GIF (*.gif)|*.gif|" +
                "TIF (*.tif)|*.tif|" +
                "PNG (*.png)|*.png";

            Nullable<bool> result = openFileDialog.ShowDialog();

            if (result == true)
            {
                saveButton.IsEnabled = true;
                retinaButton.IsEnabled = true;
                nextStepButton.IsEnabled = true;
                step = 0;

                url = openFileDialog.SafeFileName;
                this.bitmapImage = new BitmapImage(new Uri(openFileDialog.FileName));
                imageIV.Source = this.bitmapImage;
                bitmap = BitmapImage2Bitmap(this.bitmapImage);
            } 
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Title = "Save a file";
            saveFileDialog.Filter = "JPEG (*.jpg;)|*.jpg|" +
                "GIF (*.gif)|*.gif|" +
                "TIF (*.tif)|*.tif|" +
                "PNG (*.png)|*.png";

            saveFileDialog.FileName = "retina_" + url;

            Nullable<bool> result = saveFileDialog.ShowDialog();

            if (result == true)
            {
                using (var fileStream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                {
                    using (MemoryStream memory = new MemoryStream())
                    {
                        BitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(bitmapImage));
                        encoder.Save(memory);
                        memory.Position = 0;

                        switch (saveFileDialog.FileName.ToCharArray()[saveFileDialog.FileName.Length - 3])
                        {
                            case 't':
                                TiffBitmapEncoder tiff = new TiffBitmapEncoder();
                                tiff.Frames.Add(BitmapFrame.Create(memory));
                                tiff.Save(fileStream);
                                break;
                            case 'j':
                                JpegBitmapEncoder jpg = new JpegBitmapEncoder();
                                jpg.Frames.Add(BitmapFrame.Create(memory));
                                jpg.Save(fileStream);
                                break;
                            case 'g':
                                GifBitmapEncoder gif = new GifBitmapEncoder();
                                gif.Frames.Add(BitmapFrame.Create(memory));
                                gif.Save(fileStream);
                                break;
                            case 'p':
                                PngBitmapEncoder png = new PngBitmapEncoder();
                                png.Frames.Add(BitmapFrame.Create(memory));
                                png.Save(fileStream);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        private void retinaButton_Click(object sender, RoutedEventArgs e)
        {
            RetinaAlgorithm retinaAlgorithm = new RetinaAlgorithm(bitmap);
            retinaAlgorithm.Start();
            bitmap = retinaAlgorithm.GetFinalImage();
            bitmapImage = BitmapToImageSource(bitmap);
            imageIV.Source = bitmapImage;

            nextStepButton.IsEnabled = false;
            retinaButton.IsEnabled = false;
            step = 0;
        }

        public Bitmap BitmapImage2Bitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }

        public BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        private void UIElement_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                size += 0.05;
            }
            else
            {
                size -= 0.05;
            }
            imageIV.Width = bitmapImage.Width * size;
            imageIV.Height = bitmapImage.Height * size;
        }

        private void Refresh(Bitmap _bitmap)
        {
            bitmapImage = BitmapToImageSource(_bitmap);
            imageIV.Source = bitmapImage;
        }

        private void nextStepButton_Click(object sender, RoutedEventArgs e)
        {
            switch (step)
            {
                case 0:
                    retina = new RetinaAlgorithm(bitmap);
                    logger.Text = "converting to greyscale";
                    retina.ConvertToGreyScale();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 1:
                    logger.Text = "bluring";
                    retina.BlurImage();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 2:
                    logger.Text = "subtracting";
                    retina.SubtractImages();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 3:
                    logger.Text = "otsu";
                    retina.OtsuBinarization();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 4:
                    logger.Text = "median filter 3x3";
                    retina.MedianFilter();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 5:
                    logger.Text = "closing morphology";
                    retina.ClosingMorphology();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 6:
                    logger.Text = "K3M";
                    retina.K3M();
                    step++;
                    Refresh(retina.GetFinalImage());
                    break;
                case 7:
                    logger.Text = "finding and deleting minutiae";
                    retina.FindingM();
                    nextStepButton.IsEnabled = false;
                    retinaButton.IsEnabled = false;
                    Refresh(retina.GetFinalImage());
                    step = 0;
                    break;
            }
        }
    }
}
